variable "prefix" {
  description = "The prefix used for all resources in this example"
}

variable "location" {
  default = "West US"
  description = "The Azure location where all resources in this example should be created"
}
