provider "azurerm" {
  version = "=2.20.0"
  skip_provider_registration = true
  features {}

# More information on the authentication methods supported by
# the AzureRM Provider can be found here:
# http://terraform.io/docs/providers/azurerm/index.html

  # subscription_id = "4cedc5dd-e3ad-468d-bf66-32e31bdb9148"
  # client_id       = "..."
  # client_secret   = "..."
  # tenant_id       = "3617ef9b-98b4-40d9-ba43-e1ed6709cf0d"
}
