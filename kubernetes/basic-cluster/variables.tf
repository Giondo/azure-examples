variable "prefix" {
  description = "A prefix used for all resources in this example"
}

variable "location" {
  default = "South Central US"
  description = "The Azure Region in which all resources in this example should be provisioned"
}
